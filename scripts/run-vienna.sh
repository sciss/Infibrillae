#!/bin/bash
cd "$(dirname "$0")"
cd ..
echo "in|fibrillae (ParallelVienna)"

# xrandr
xrandr --output HDMI-1 --mode 640x480

sleep 4
# turn off screen blanking
xset s off
xset -dpms

echo "- Setting ALSA controls"
# amixer -c1 set Speaker Playback 16  # -21 dB
amixer -c1 set Speaker Playback 9  # -28 dB
amixer -c1 set Mic Capture 12 # 0 dB

echo "- wait a few seconds to be able to interrupt"
sleep 12

echo "- start qjackctl"
qjackctl &

sleep 10
echo "- start mellite"
mellite-launcher --headless --boot-audio --auto-run start /home/pi/Documents/projects/infibrillae/workspace.mllt &

sleep 30
echo "- start visual"
java -jar jvm/infibrillae.jar --full-screen --init-delay 0 --shutdown 0 --off-switch
