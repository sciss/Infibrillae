/*
 *  ModelVar.scala
 *  (in|fibrillae)
 *
 *  Copyright (c) 2020-2021 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.infibrillae

import de.sciss.model.Model
import de.sciss.model.impl.ModelImpl

object ModelVar {
  def apply[A](init: A): ModelVar[A] = new VarImpl(init)

  private final class VarImpl[A](init: A) extends ModelVar[A] with ModelImpl[A] {
    private var _value = init

    private val sync = new AnyRef

    override def apply(): A = sync.synchronized(_value)

    override def update(value: A): Unit = {
      val change = sync.synchronized {
        (_value != value) && {
          _value = value
          true
        }
      }
      if (change) dispatch(value)
    }
  }
}
trait ModelVar[A] extends Model[A] {
  def update(value: A): Unit
  def apply(): A
}

