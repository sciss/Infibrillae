// version: 24-Jul-2021

val DEBUG = false

val NUM_CH = 4

//val pBoost  = pAudio("gain" , ParamSpec( 0.1, 10, ExpWarp), default(1.0))

//val sig0    = ScanInFix(2)
val sig0    = In.ar(NumOutputBuses.ir, 1) * 30.dbAmp

//val sig     = sig0 // LeakDC.ar(sig0)
//val sigL    = sig.out(0)
//val sigR    = sig.out(1)
//// we can't get a direct n_map, so let's supply the bus as argument
////val vBalBus = Attribute.ar("$bal-bus", 0.0, fixed = true)
//val vBalBus = "pan-idx".kr(0.0) * 0.25 // comes in at 0...4
//
//val vBal    = In.kr(vBalBus)
//val vBoost  = 1.0 // pBoost.out(0)
//val balance = Balance2.ar(sigL, sigR, pos = vBal, level = vBoost)
//val sum     = balance.left + balance.right
//val sig1: GE = sum // ForceChan(sum)
val sig1: GE = sig0.clip2(1.0) // ForceChan(sum)
          
val in    = sig1
val pLo   = "lo" .kr(Seq.fill(NUM_CH)(0.0)) // pAudio("lo"   , ParamSpec(0.0, 1.0), default(0.0))
val pHi   = "hi" .kr(Seq.fill(NUM_CH)(1.0)) // pAudio("hi"   , ParamSpec(0.0, 1.0), default(1.0))
val pRect = "abs".kr(Seq.fill(NUM_CH)(0.0)) // pAudio("abs"  , ParamSpec(0.0, 1.0, IntWarp), default(0.0))
//val pGain = pAudio("gain" , ParamSpec(-30, 30), default(0.0))
//val pMix  = mkMix()

if (DEBUG) {
  pLo   .poll(0.5, "adapt-lo ")
  pHi   .poll(0.5, "adapt-hi ")
  pRect .poll(0.5, "adapt-abs")
}

val inG   = in // * pGain.dbAmp
val flt1  = inG.max(0.0)
val flt2  = inG.abs
val flt3  = Select.ar(pRect, flt1 :: flt2 :: Nil)
val flt   = flt3 * (pHi - pLo) + pLo
// mix(in, flt, pMix)
ScanOut(flt)
